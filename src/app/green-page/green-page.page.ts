import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ShareItComponent } from '../share-it/share-it.component';


@Component({
  selector: 'app-green-page',
  templateUrl: './green-page.page.html',
  styleUrls: ['./green-page.page.scss'],
})
export class GreenPagePage implements OnInit {


  coupleNames: [] = [];
  constructor(private modalCtrl: ModalController) {
    this.coupleNames = ["saifeena", "sufina", "kersaif", "xyz", "abc"];
  }

  ngOnInit() {

  }
  async openModal() {
    const modal = await this.modalCtrl.create({
      component: ShareItComponent
    });
    modal.present();
  }

}
