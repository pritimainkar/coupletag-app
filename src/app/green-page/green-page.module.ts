import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GreenPagePageRoutingModule } from './green-page-routing.module';

import { GreenPagePage } from './green-page.page';


import { ShareItComponent } from '../share-it/share-it.component';
import { MenuComponent } from '../menu/menu.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GreenPagePageRoutingModule
  ],
  entryComponents: [ShareItComponent],
  declarations: [GreenPagePage, ShareItComponent, MenuComponent]
})
export class GreenPagePageModule {






}


