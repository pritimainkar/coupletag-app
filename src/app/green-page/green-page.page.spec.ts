import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GreenPagePage } from './green-page.page';



 
describe('GreenPagePage', () => {
  let component: GreenPagePage;
  let fixture: ComponentFixture<GreenPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GreenPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GreenPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
