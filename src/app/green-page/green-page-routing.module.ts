import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GreenPagePage } from './green-page.page';

const routes: Routes = [
  {
    path: '',
    component: GreenPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GreenPagePageRoutingModule {}
