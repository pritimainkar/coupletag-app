import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'enter-name/:id',
    loadChildren: () => import('./enter-name/enter-name.module').then( m => m.EnterNamePageModule)
  },
  {
    path: 'green-page',
    loadChildren: () => import('./green-page/green-page.module').then( m => m.GreenPagePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
